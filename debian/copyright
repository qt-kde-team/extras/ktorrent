Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ktorrent
Upstream-Contact: kde-devel@kde.org
Source: https://invent.kde.org/network/ktorrent

Files: *
Copyright: 2017-2020, Alexander Trufanov <trufanovan@gmail.com>
           2005-2009, Ivan Vasic <ivasic@gmail.com>
           2005, Ivan Vasic ivasic@gmail.com
           2006-2007, Ivan Vasić <ivasic@gmail.com>
           2010, Jonas Lundqvist <jonas@gannon.se>
           2005-2012, Joris Guisson <joris.guisson@gmail.com>
           2005, Joris Guisson joris.guisson@gmail.com
           2014, Juan Palacios <jpalaciosdev@gmail.com>
           2007, Krzysztof Kundzicz <athantor@gmail.com>
           2020, Madhav Kanbur <abcdjdj@gmail.com>
           2007, Modestas Vainius <modestas@vainius.eu>
           2005-2007, Vincent Wagelaar <vincent@ricardis.tudelft.nl>
License: GPL-2.0-or-later

Files: ktorrent/org.kde.ktorrent.appdata.xml
Copyright: 2017, Burkhard Lück <lueck@hube-lueck.de>
License: FSFAP

Files: plugins/search/magneturlschemehandler.cpp
       plugins/search/magneturlschemehandler.h
Copyright: 2019, Gilbert Assaf <gassaf@gmx.de>
License: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

Files: plugins/shutdown/shutdowntorrentmodel.h
Copyright: 2009, Joris Guisson <joris.guisson@gmail.com>
License: LGPL-2.0-only

Files: ktorrent/statusbarofflineindicator.cpp
       ktorrent/statusbarofflineindicator.h
Copyright: 2007, Will Stephenson <wstephenson@kde.org>
License: LGPL-2.0-only_WITH_Qt-Commercial-exception-1.0

Files: debian/*
Copyright: 2007-2022, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
License: GPL-2.0-or-later

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.

License: GPL-2.0-only
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: GPL-3.0-only
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GPL License as published by the Free
 Software Foundation, version 3.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in
 `/usr/share/common-licenses/GPL-3’.

License: LGPL-2.0-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version
 2 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.0-only_WITH_Qt-Commercial-exception-1.0
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version
 2 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.
 .
 As a special exception, the copyright holder(s) give permission to
 link this program with the Qt Library (commercial or non-commercial
 edition), and distribute the resulting executable, without including
 the source code for the Qt library in the source distribution.

License: LicenseRef-KDE-Accepted-GPL
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the license or (at
 your option) at any later version that is accepted by the membership
 of KDE e.V. (or its successor approved by the membership of KDE
 e.V.), which shall act as a proxy as defined in Section 14 of
 version 3 of the license.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
